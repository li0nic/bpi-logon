# BPi Login

This tools shows you some important information about your device on logon.
Inspired by the ARMBIAN logon, this is now brought to your BananaPi SBC to use
with the standard images from BananaPi itself.

For installation:

apt install git && git clone https://li0nic@gitlab.com/li0nic/bpi-logon.git

cd /root/bpi-logon && chmod +x install-m.sh

than run:

./install-m.sh

Reboot now and re-login. You should now be presented by a nice Login-Screen

If you're changing your hostname, please run this command to update the hostname file and reboot afterwards.

echo 'hostname -f' | toilet -f standard -F metal > /etc/motd
